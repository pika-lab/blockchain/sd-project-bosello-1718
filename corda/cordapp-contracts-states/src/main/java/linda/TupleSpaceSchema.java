package linda;

import java.util.List;
import javax.persistence.*;
import com.google.common.collect.ImmutableList;

import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import net.corda.core.serialization.ConstructorForDeserialization;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public class TupleSpaceSchema extends MappedSchema {
    public TupleSpaceSchema() {
        super(TupleSpaceSchema.class, 1, ImmutableList.of(TupleSpaceState.class));
    }

    @Entity
    @Table(name = "tuplespace_states")
    public static class TupleSpaceState extends PersistentState {
    
        private static final long serialVersionUID = 1L;
        @Column(name = "tuple") private final String tuple;
        @Column(name = "producer") private final String producer;
        @ElementCollection
        @Column(name="parties")
        private final List<String> parties;

        // Default constructor required by hibernate.
        public TupleSpaceState(){
            this.tuple = "";
            this.producer = null;
            this.parties = null;
        }
    
        @ConstructorForDeserialization
        public TupleSpaceState(String tuple, String producer, List<String> parties) {
            this.tuple = tuple;
            this.producer = producer;
            this.parties = parties;
        }
    
        public String getTuple() {
            return tuple;
        }
    
        public String getProducer() {
            return producer;
        }
    
        public List<String> getParties() {
            return parties;
        }
    }
}
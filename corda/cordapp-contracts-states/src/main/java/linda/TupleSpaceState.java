package linda;

import java.util.ArrayList;
import java.util.List;
import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.schemas.MappedSchema;
import net.corda.core.schemas.PersistentState;
import net.corda.core.schemas.QueryableState;
import com.google.common.collect.ImmutableList;

public class TupleSpaceState implements ContractState, QueryableState {

    private final String tuple;
    private final Party producer;
    private final List<Party> parties;
    private final List<AbstractParty> abstractParties;
    private final List<String> stringParties;

    public TupleSpaceState(String tuple, Party producer, List<Party> parties) {
        this.tuple = tuple;
        this.producer = producer;
        this.parties = parties;
        abstractParties = new ArrayList<>();
        stringParties = new ArrayList<>();
        for (Party p : parties) {
            abstractParties.add(p);
            stringParties.add(p.getName().toString());
        }
    }

    public String getTuple() {
        return tuple;
    }

    public Party getProducer() {
        return producer;
    }

    public List<Party> getParties() {
        return parties;
    }

    @Override
    public List<AbstractParty> getParticipants() {
        return abstractParties;
    }

	@Override
	public PersistentState generateMappedObject(MappedSchema schema) {
		if (schema instanceof TupleSpaceSchema) {
            return new TupleSpaceSchema.TupleSpaceState(
                    this.tuple,
                    this.producer.getName().toString(),
                    this.stringParties);
        } else {
            throw new IllegalArgumentException("Unrecognised schema $schema");
        }
	}

	@Override
	public Iterable<MappedSchema> supportedSchemas() {
		return ImmutableList.of(new TupleSpaceSchema());
	}
}
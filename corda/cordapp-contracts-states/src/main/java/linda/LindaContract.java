package linda;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;
import net.corda.core.contracts.CommandWithParties;

import java.security.PublicKey;
import java.util.List;

import static net.corda.core.contracts.ContractsDSL.requireSingleCommand;
import static net.corda.core.contracts.ContractsDSL.requireThat;

/**
 * Define your contract here.
 */
public class LindaContract implements Contract {
    // This is used to identify our contract when building a transaction.
    public static final String LINDA_CONTRACT_ID = "linda.LindaContract";

    /**
     * A transaction is considered valid if the verify() function of the contract of
     * each of the transaction's input and output states does not throw an
     * exception.
     */
    @Override
    public void verify(LedgerTransaction tx) {
        final CommandWithParties<LindaContract.Commands> command = requireSingleCommand(tx.getCommands(),
                LindaContract.Commands.class);

        requireThat(check -> {
            if (command.getValue() instanceof Commands.Put) {
                // Constraints on the shape of the transaction.
                check.using("No inputs should be consumed when you put tuple.", tx.getInputs().isEmpty());
                check.using("There should be one output state.", tx.getOutputs().size() == 1);

                final TupleSpaceState out = tx.outputsOfType(TupleSpaceState.class).get(0);

                // Constraints on the signers.
                final List<PublicKey> signers = command.getSigners();
                check.using("There must be one signer.", signers.size() == 1);
                check.using("The producer must be signer.",
                        signers.contains(out.getProducer().getOwningKey()));

            } else if (command.getValue() instanceof Commands.Take) {
                // Constraints on the shape of the transaction.
                check.using("One input should be consumed when you take a tuple.", tx.getInputs().size() == 1);
                check.using("No outputs should be produced.", tx.getOutputs().isEmpty());

                // specific constraints.
                final TupleSpaceState in = tx.inputsOfType(TupleSpaceState.class).get(0);

                // Constraints on the signers.
                final List<PublicKey> signers = command.getSigners();
                check.using("The producer must be signer.",
                        signers.contains(in.getProducer().getOwningKey()));
            }

            return null;
        });
    }

    public static class Commands implements CommandData {
        public static class Take extends Commands {
            @Override
            public boolean equals(Object obj) {
                return obj instanceof Take;
            }
        }

        public static class Put extends Commands {
            @Override
            public boolean equals(Object obj) {
                return obj instanceof Put;
            }
        }
    }
}
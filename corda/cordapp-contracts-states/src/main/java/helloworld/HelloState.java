package helloworld;

import net.corda.core.contracts.ContractState;
import net.corda.core.identity.AbstractParty;

import java.util.List;

import com.google.common.collect.ImmutableList;
import net.corda.core.identity.Party;

public class HelloState implements ContractState {

    private final String greetings;
    private final Party sender;
    private final Party receiver;

    public HelloState(String greetings, Party sender, Party receiver) {
        this.greetings = greetings;
        this.sender = sender;
        this.receiver = receiver;
    }

    public String getGreetings() {
        return greetings;
    }

    public Party getSender() {
        return sender;
    }

    public Party getReceiver() {
        return receiver;
    }

    @Override
    public List<AbstractParty> getParticipants() {
        return ImmutableList.of(sender, receiver);
    }
}
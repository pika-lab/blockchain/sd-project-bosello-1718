package helloworld;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;
import com.google.common.collect.ImmutableList;
import net.corda.core.contracts.CommandWithParties;
import net.corda.core.identity.Party;

import java.security.PublicKey;
import java.util.List;

import static net.corda.core.contracts.ContractsDSL.requireSingleCommand;
import static net.corda.core.contracts.ContractsDSL.requireThat;

/**
 * Define your contract here.
 */
public class HelloContract implements Contract {
    // This is used to identify our contract when building a transaction.
    public static final String HELLO_CONTRACT_ID = "helloworld.HelloContract";

    /**
     * A transaction is considered valid if the verify() function of the contract of each of the transaction's input
     * and output states does not throw an exception.
     */
    @Override
    public void verify(LedgerTransaction tx) {
        final CommandWithParties<HelloContract.Create> command = requireSingleCommand(tx.getCommands(), HelloContract.Create.class);

        requireThat(check -> {
            // Constraints on the shape of the transaction.
            check.using("No inputs should be consumed when you greet.", tx.getInputs().isEmpty());
            check.using("There should be one output state of type HelloState.", tx.getOutputs().size() == 1);

            // specific constraints.
            final HelloState out = tx.outputsOfType(HelloState.class).get(0);
            final Party sender = out.getSender();
            final Party receiver = out.getReceiver();
            check.using("The greetings must contain hello.", out.getGreetings().contains("hello"));
            check.using("The sender and the receiver cannot be the same entity.", sender != receiver);

            // Constraints on the signers.
            final List<PublicKey> signers = command.getSigners();
            check.using("There must be two signers.", signers.size() == 2);
            check.using("The sender and receiver must be signers.", signers.containsAll(
                    ImmutableList.of(sender.getOwningKey(), receiver.getOwningKey())));

            return null;
        });
    }

    public static class Create implements CommandData {
    }
}
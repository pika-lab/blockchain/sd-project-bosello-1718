package linda.flow;

import co.paralleluniverse.fibers.Suspendable;
import linda.TupleSpaceState;
import net.corda.core.contracts.ContractState;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.utilities.ProgressTracker;

import static net.corda.core.contracts.ContractsDSL.requireThat;

@InitiatedBy(PutFlow.class)
public class PutFlowResponder extends FlowLogic<Void> {
    private final FlowSession otherPartySession;

    public PutFlowResponder(FlowSession otherPartySession) {
        this.otherPartySession = otherPartySession;
    }

    @Suspendable
    @Override
    public Void call() throws FlowException {
        class SignTxFlow extends SignTransactionFlow {
            private SignTxFlow(FlowSession otherPartySession, ProgressTracker progressTracker) {
                super(otherPartySession, progressTracker);
            }

            @Override
            protected void checkTransaction(SignedTransaction stx) {
                requireThat(require -> {
                    ContractState output = stx.getTx().getOutputs().get(0).getData();
                    require.using("This must be a tuplespace transaction.", output instanceof TupleSpaceState);
                    TupleSpaceState tuple = (TupleSpaceState) output;
                    require.using("The tuple must be mine", tuple.getProducer().equals(getOurIdentity()));
                    return null;
                });
            }
        }

        subFlow(new SignTxFlow(otherPartySession, SignTransactionFlow.Companion.tracker()));

        return null;
    }
}
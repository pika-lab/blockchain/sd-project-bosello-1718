package linda.flow.notworking;

import java.lang.reflect.Field;
import java.security.PublicKey;
import java.util.List;

import com.google.common.collect.ImmutableList;

import co.paralleluniverse.fibers.Suspendable;
import co.paralleluniverse.strands.concurrent.CountDownLatch;
import linda.LindaContract;
import linda.TupleSpaceSchema;
import linda.TupleSpaceState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.CollectSignaturesFlow;
import net.corda.core.flows.FinalityFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.identity.Party;
import net.corda.core.messaging.DataFeed;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.CriteriaExpression;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultQueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import net.corda.core.node.services.vault.Builder;

@InitiatingFlow
@StartableByRPC
public class BlockingTakeFlow extends FlowLogic<Void> {
    private final String tuple;
    private final CountDownLatch latch = new CountDownLatch(1);
    StateAndRef<TupleSpaceState> selectedTuple;
    TupleSpaceState selectedTupleData;
    private boolean complete = false;

    public BlockingTakeFlow(String tuple) {
        this.tuple = tuple;
    }

    private final ProgressTracker progressTracker = new ProgressTracker();

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public Void call() throws FlowException {
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);
        final TransactionBuilder txBuilder = new TransactionBuilder();
        txBuilder.setNotary(notary);

        VaultQueryCriteria unconsumedCriteria = new VaultQueryCriteria(Vault.StateStatus.UNCONSUMED);
        Field attributeTuple = null;
        try {
            attributeTuple = TupleSpaceSchema.TupleSpaceState.class.getDeclaredField("tuple");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CriteriaExpression tupleIndex = Builder.equal(attributeTuple, tuple);
        QueryCriteria tupleCriteria = new VaultCustomQueryCriteria(tupleIndex);
        QueryCriteria criteria = unconsumedCriteria.and(tupleCriteria);
        System.out.println("Created tx and criteria");

        final DataFeed<Vault.Page<TupleSpaceState>, Vault.Update<TupleSpaceState>> dataFeed = getServiceHub()
                .getVaultService().trackBy(TupleSpaceState.class, criteria);
        final Vault.Page<TupleSpaceState> snapshot = dataFeed.getSnapshot();
        final Observable<Vault.Update<TupleSpaceState>> updates = dataFeed.getUpdates();
        System.out.println("Created updates stream");

        snapshot.getStates().forEach(this::acquireTuple);
        Subscription observer = updates.observeOn(Schedulers.io()).subscribe(update -> update.getProduced().forEach(this::acquireTuple));
        System.out.println("subcribed update event with io scheduler, flow thread waiting for tuple");

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Latch released: found tuple");
        observer.unsubscribe();

        List<PublicKey> requiredSigners = ImmutableList.of(getOurIdentity().getOwningKey(),
                selectedTupleData.getProducer().getOwningKey());
        Command cmd = new Command<>(new LindaContract.Commands.Take(), requiredSigners);

        txBuilder.withItems(selectedTuple, cmd);
        System.out.println("Tx builded");

        txBuilder.verify(getServiceHub());
        System.out.println("Tx verified");

        // Signing the transaction.
        final SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);
        System.out.println("Tx signed");

        if (!selectedTupleData.getProducer().equals(getOurIdentity())) {
            System.out.println("Request producer sign");
            // Creating a session with the other party.
            FlowSession otherpartySession = initiateFlow(selectedTupleData.getProducer());
            System.out.println("Session with producer opened");
            // Obtaining the counterparty's signature.
            SignedTransaction fullySignedTx = subFlow(new CollectSignaturesFlow(signedTx,
                    ImmutableList.of(otherpartySession), CollectSignaturesFlow.tracker()));
            System.out.println("Received producer sign");
            // Finalising the transaction.
            subFlow(new FinalityFlow(fullySignedTx));
            System.out.println("Complete");
        } else {
            // Finalising the transaction.
            subFlow(new FinalityFlow(signedTx));
            System.out.println("Complete");
        }

        return null;
    }

    private void acquireTuple(StateAndRef<TupleSpaceState> selectedTuple) {
        try {
            if (!complete) {
                System.out.println("update/snapshot handler");
                selectedTupleData = selectedTuple.component1().getData();
                if (selectedTupleData.getTuple().equals(tuple)) {
                    this.selectedTuple = selectedTuple;
                    complete = true;
                    latch.countDown();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
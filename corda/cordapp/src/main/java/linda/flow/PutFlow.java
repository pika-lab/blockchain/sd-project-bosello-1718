package linda.flow;

import java.util.List;

import co.paralleluniverse.fibers.Suspendable;
import linda.LindaContract;
import linda.TupleSpaceState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.CommandData;
import net.corda.core.flows.FinalityFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.core.utilities.ProgressTracker.Step;

@InitiatingFlow
@StartableByRPC
public class PutFlow extends FlowLogic<Void> {
    private final String tuple;
    private final List<Party> parties;

    public PutFlow(String tuple, List<Party> parties) {
        this.tuple = tuple;
        this.parties = parties;
    }

    private static final Step CREATE_TX = new Step("Created transaction and tx builder.");
    private static final Step SIGNED = new Step("Self Signed.");
    private static final Step FINALIZED = new Step("Finalized.");

    private final ProgressTracker progressTracker = new ProgressTracker(
        CREATE_TX,
        SIGNED,
        FINALIZED);

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    /**
     * The flow logic is encapsulated within the call() method.
     */
    @Suspendable
    @Override
    public Void call() throws FlowException {
        // We retrieve the notary identity from the network map.
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);
        
        // We create the transaction components.
        TupleSpaceState outputState = new TupleSpaceState(tuple, getOurIdentity(), parties);
        CommandData cmdType = new LindaContract.Commands.Put();
        Command cmd = new Command<>(cmdType, getOurIdentity().getOwningKey());

        // We create a transaction builder and add the components.
        final TransactionBuilder txBuilder = new TransactionBuilder(notary)
                .addOutputState(outputState, LindaContract.LINDA_CONTRACT_ID)
                .addCommand(cmd);
        progressTracker.setCurrentStep(CREATE_TX);

        // Signing the transaction.
        final SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);
        progressTracker.setCurrentStep(SIGNED);

        // Finalising the transaction.
        subFlow(new FinalityFlow(signedTx));
        progressTracker.setCurrentStep(FINALIZED);

        return null;
    }
}
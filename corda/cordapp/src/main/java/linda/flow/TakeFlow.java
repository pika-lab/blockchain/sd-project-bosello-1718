package linda.flow;

import java.lang.reflect.Field;
import java.security.PublicKey;
import java.util.List;

import com.google.common.collect.ImmutableList;

import co.paralleluniverse.fibers.Suspendable;
import linda.LindaContract;
import linda.TupleSpaceSchema;
import linda.TupleSpaceState;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.CollectSignaturesFlow;
import net.corda.core.flows.FinalityFlow;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.FlowSession;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.identity.Party;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.Vault.Page;
import net.corda.core.node.services.vault.CriteriaExpression;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultQueryCriteria;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import net.corda.core.utilities.ProgressTracker.Step;
import net.corda.core.node.services.vault.Builder;

@InitiatingFlow
@StartableByRPC
public class TakeFlow extends FlowLogic<String> {
    private final String tuple;

    public TakeFlow(String tuple) {
        this.tuple = tuple;
    }

    private static final Step INIT = new Step("Created tx builder and criteria.");
    private static final Step GET_RESULT = new Step("Retrived result.");
    private static final Step TUPLE_FOUND = new Step("Metching tuple found.");
    private static final Step NO_TUPLE = new Step("No tuple found.");
    private static final Step VERIFY = new Step("Transaction verified.");
    private static final Step SIGN = new Step("Transaction signed.");
    private static final Step REQUEST_OTHER = new Step("Starting flow to request producer sign.");
    private static final Step REQUEST_OTHER_COMPLETE = new Step("Received producer sign.");
    private static final Step NO_OTHER = new Step("Producer and consumer are the same, no need of other sign.");
    private static final Step END = new Step("Flow completed.");

    private final ProgressTracker progressTracker = new ProgressTracker(
        /*INIT,
        GET_RESULT,
        TUPLE_FOUND,
        NO_TUPLE,
        VERIFY,
        SIGN,
        REQUEST_OTHER,
        REQUEST_OTHER_COMPLETE,
        NO_OTHER,
        END*/);

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    /**
     * The flow logic is encapsulated within the call() method.
     */
    @Suspendable
    @Override
    public String call() throws FlowException {
        // We retrieve the notary identity from the network map.
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        // We create a transaction builder.
        final TransactionBuilder txBuilder = new TransactionBuilder();
        txBuilder.setNotary(notary);

        VaultQueryCriteria unconsumedCriteria = new VaultQueryCriteria(Vault.StateStatus.UNCONSUMED);
        Field attributeTuple = null;
        try {
            attributeTuple = TupleSpaceSchema.TupleSpaceState.class.getDeclaredField("tuple");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CriteriaExpression tupleIndex = Builder.equal(attributeTuple, tuple);
        QueryCriteria tupleCriteria = new VaultCustomQueryCriteria(tupleIndex);
        QueryCriteria criteria = unconsumedCriteria.and(tupleCriteria);
        //progressTracker.setCurrentStep(INIT);
        System.out.println(INIT.getLabel());

        Page<TupleSpaceState> results = getServiceHub().getVaultService().queryBy(TupleSpaceState.class, criteria);
        List<StateAndRef<TupleSpaceState>> tupleResult = results.getStates();
        //progressTracker.setCurrentStep(GET_RESULT);
        System.out.println(GET_RESULT.getLabel());

        if (tupleResult.size() > 0) {
            StateAndRef<TupleSpaceState> selectedTuple = tupleResult.get(0);
            TupleSpaceState selectedTupleData = selectedTuple.component1().getData();
            //progressTracker.setCurrentStep(TUPLE_FOUND);
            System.out.println(TUPLE_FOUND.getLabel());

            List<PublicKey> requiredSigners = ImmutableList.of(getOurIdentity().getOwningKey(), selectedTupleData.getProducer().getOwningKey());
            Command cmd = new Command<>(new LindaContract.Commands.Take(), requiredSigners);

            // We add the items to the builder.
            txBuilder.withItems(selectedTuple, cmd);

            // Verifying the transaction.
            txBuilder.verify(getServiceHub());
            //progressTracker.setCurrentStep(VERIFY);
            System.out.println(VERIFY.getLabel());

            // Signing the transaction.
            final SignedTransaction signedTx = getServiceHub().signInitialTransaction(txBuilder);
            //progressTracker.setCurrentStep(SIGN);
            System.out.println(SIGN.getLabel());

            if (!selectedTupleData.getProducer().equals(getOurIdentity())) {
                //progressTracker.setCurrentStep(REQUEST_OTHER);
                System.out.println(REQUEST_OTHER.getLabel());
                // Creating a session with the other party.
                FlowSession otherpartySession = initiateFlow(selectedTupleData.getProducer());

                // Obtaining the counterparty's signature.
                SignedTransaction fullySignedTx = subFlow(new CollectSignaturesFlow(signedTx,
                        ImmutableList.of(otherpartySession), CollectSignaturesFlow.tracker()));

                //progressTracker.setCurrentStep(REQUEST_OTHER_COMPLETE);
                System.out.println(REQUEST_OTHER_COMPLETE.getLabel());
                // Finalising the transaction.
                subFlow(new FinalityFlow(fullySignedTx));
                return selectedTupleData.getTuple();
            } else {
                //progressTracker.setCurrentStep(NO_OTHER);
                System.out.println(NO_OTHER.getLabel());
                // Finalising the transaction.
                subFlow(new FinalityFlow(signedTx));
                return selectedTupleData.getTuple();
            }
        } else {
            //progressTracker.setCurrentStep(NO_TUPLE);
            System.out.println(NO_TUPLE.getLabel());
            throw new FlowException(NO_TUPLE.getLabel());
        }
    }
}
package linda.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import net.corda.client.rpc.CordaRPCClient;
import net.corda.client.rpc.CordaRPCClientConfiguration;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.utilities.NetworkHostAndPort;

public class LindaClient {

    private static final String HOST = "localhost";
    private static final String USERNAME = "user1";
    private static final String PASSWORD = "test";
    private static final String PARTYA = "O=PartyA,L=London,C=GB";
    private static final String PARTYB = "O=PartyB,L=New York,C=US";

    private static final Scanner CONSOLE = new Scanner(System.in);

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        final NetworkHostAndPort nodeAddress = new NetworkHostAndPort(HOST, port);
        final CordaRPCClient client = new CordaRPCClient(nodeAddress, CordaRPCClientConfiguration.DEFAULT);
        final CordaRPCOps proxy = client.start(USERNAME, PASSWORD).getProxy();

        List<Party> parties = new ArrayList<>();
        parties.add(stringToParty(proxy, PARTYA));
        parties.add(stringToParty(proxy, PARTYB));

        LindaRPC tuplespace = new LindaRPC(proxy, parties);
        String tt;
        String res;

        while (true) {
            System.out.println("Select operation [out, in, rd, inp, rdp]");
            String input = CONSOLE.nextLine();
            System.out.println("Enter tuple");
            try {
                switch (input) {
                case "out":
                    tt = CONSOLE.nextLine();
                    tuplespace.out(tt);
                    System.out.println("done");
                    break;
                case "in":
                    tt = CONSOLE.nextLine();
                    res = tuplespace.in(tt);
                    System.out.println("Got tuple: " + res);
                    break;
                case "rd":
                    tt = CONSOLE.nextLine();
                    res = tuplespace.rd(tt);
                    System.out.println("Got tuple: " + res);
                    break;
                case "inp":
                    tt = CONSOLE.nextLine();
                    res = tuplespace.inp(tt);
                    if (res != null) {
                        System.out.println("Got tuple: " + res);
                    } else {
                        System.out.println("tuple not found");
                    }
                    break;
                case "rdp":
                    tt = CONSOLE.nextLine();
                    res = tuplespace.rdp(tt);
                    if (res != null) {
                        System.out.println("Got tuple: " + res);
                    } else {
                        System.out.println("tuple not found");
                    }
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private static Party stringToParty(CordaRPCOps proxy, String partyString) {
        CordaX500Name x500Name = CordaX500Name.parse(partyString);
        return proxy.wellKnownPartyFromX500Name(x500Name);
    }

}
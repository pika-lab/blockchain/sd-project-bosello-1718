package linda.client;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import linda.TupleSpaceSchema;
import linda.TupleSpaceState;
import linda.flow.PutFlow;
import linda.flow.TakeFlow;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.identity.Party;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.messaging.DataFeed;
import net.corda.core.messaging.FlowHandle;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.CriteriaExpression;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultQueryCriteria;
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria;
import net.corda.core.node.services.vault.Builder;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;

public class LindaRPC {

    final CordaRPCOps proxy;
    final List<Party> parties;

    private final Semaphore tupleFoundEvent = new Semaphore(0);
    AtomicBoolean complete = new AtomicBoolean(false);
    TupleSpaceState selectedTupleData;

    public LindaRPC(CordaRPCOps proxy, List<Party> parties) {
        this.proxy = proxy;
        this.parties = parties;
    }

    public void out(String tuple) throws InterruptedException, ExecutionException {
        FlowHandle<Void> flowHandle = proxy.startFlowDynamic(PutFlow.class, tuple, parties);
        flowHandle.getReturnValue().get();
    }

    public String in(String tupleTeplate) throws InterruptedException {
        complete.set(false);
        Subscription observer = observeCurrentAndFutureTuple(tupleTeplate);
        String flowResult = "";

        while (flowResult.equals("")) {
            tupleFoundEvent.acquire();
            FlowHandle<String> flowHandle = proxy.startFlowDynamic(TakeFlow.class, tupleTeplate);
            try {
                flowResult = flowHandle.getReturnValue().get();
            } catch (ExecutionException e) {
                flowResult = "";
            }
        }

        observer.unsubscribe();
        return flowResult;
    }

    public String rd(String tupleTeplate) throws InterruptedException {
        complete.set(false);
        Subscription observer = observeCurrentAndFutureTuple(tupleTeplate);
        tupleFoundEvent.acquire();
        observer.unsubscribe();
        return selectedTupleData.getTuple();
    }

    public String inp(String tupleTeplate) throws InterruptedException {
        FlowHandle<String> flowHandle = proxy.startFlowDynamic(TakeFlow.class, tupleTeplate);
        try {
            return flowHandle.getReturnValue().get();
        } catch (ExecutionException e) {
            return null;
        }
    }

    public String rdp(String tupleTeplate) {
        complete.set(false);
        observeCurrentTuple(tupleTeplate);
        if (complete.get()) {
            try {
                tupleFoundEvent.tryAcquire(100, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
            }
            return selectedTupleData.getTuple();
        } else {
            return null;
        }
    }

    private DataFeed<Vault.Page<TupleSpaceState>, Vault.Update<TupleSpaceState>> getTupleDataFeed(String tuple) {
        VaultQueryCriteria unconsumedCriteria = new VaultQueryCriteria(Vault.StateStatus.UNCONSUMED);
        Field attributeTuple = null;
        try {
            attributeTuple = TupleSpaceSchema.TupleSpaceState.class.getDeclaredField("tuple");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CriteriaExpression tupleIndex = Builder.equal(attributeTuple, tuple);
        QueryCriteria tupleCriteria = new VaultCustomQueryCriteria(tupleIndex);
        QueryCriteria criteria = unconsumedCriteria.and(tupleCriteria);

        return proxy.vaultTrackByCriteria(TupleSpaceState.class, criteria);
    }

    private void observeCurrentTuple(String tuple) {
        DataFeed<Vault.Page<TupleSpaceState>, Vault.Update<TupleSpaceState>> dataFeed = getTupleDataFeed(tuple);
        final Vault.Page<TupleSpaceState> snapshot = dataFeed.getSnapshot();

        snapshot.getStates().forEach(update -> acquireTuple(update, tuple));
    }

    private Subscription observeCurrentAndFutureTuple(String tuple) {
        DataFeed<Vault.Page<TupleSpaceState>, Vault.Update<TupleSpaceState>> dataFeed = getTupleDataFeed(tuple);
        final Vault.Page<TupleSpaceState> snapshot = dataFeed.getSnapshot();
        final Observable<Vault.Update<TupleSpaceState>> updates = dataFeed.getUpdates();

        snapshot.getStates().forEach(update -> acquireTuple(update, tuple));
        return updates.observeOn(Schedulers.io())
                .subscribe(update -> update.getProduced().forEach(res -> acquireTuple(res, tuple)));
    }

    private void acquireTuple(StateAndRef<TupleSpaceState> selectedTuple, String tuple) {
        if (!complete.get()) {
            selectedTupleData = selectedTuple.component1().getData();
            if (selectedTupleData.getTuple().equals(tuple)) {
                complete.set(true);
                tupleFoundEvent.release();
            }
        }
    }

}
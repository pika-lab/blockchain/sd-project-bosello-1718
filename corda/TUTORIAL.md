# execution environment
Oracle JDK 8 JVM (Not working with Java 10)

# Execution
## Hello World
### Building the CorDapp:

**Unix:** 

     ./gradlew deployNodes

**Windows:**

     gradlew.bat deployNodes

> you may get error caused by stale Gradle processes, in that case use *killall java -9* or *pkill java* on Unix, or *wmic process where "name like '%java%'" delete* in Windows, to kill all Java processes.

Note: You'll need to re-run this build step after making any changes to
the template for these to take effect on the node.

### Running the Nodes

The Gradle build script will have created a folder for each node. You'll
see three folders in build/nodes, one for each node and a `runnodes` script. You can
run the nodes with:

**Unix:**

     build/nodes/runnodes

**Windows:**

    build/nodes/runnodes.bat

You should now have three Corda nodes running on your machine serving 
the template.

When the nodes have booted up, you should see a message like the following 
in the console: 

     Node started up and registered in 5.007 sec

From the node shell send greetings with commend:

    start HelloFlow greetings: "hello dear friend", receiver: "O=PartyB,L=New York,C=US"

>To test API the following constraints are imposed:  
>The contract require that greetings contains "hello".  
>The receiver flow require that greetings contains "dear" to sign the transaction.

We can check the contents of each node’s vault by running:

    run vaultQuery contractStateType: helloworld.HelloState

### Note
Start point:
https://docs.corda.net/tutorials-index.html

## Linda
Run

    ./runCordapp.sh

 This script will automate the startup process:
+ Clean the enviroment (delete build directories, **kill java processes**)
+ Build the CorDapp
+ Run the nodes

>This will start a terminal window for each node, and an additional terminal window for each node’s webserver. Since webserver is not implemented, an exception will be thrown, you can close these terminal windows and keep only Corda's built-in "CRaSH" shells.

Wait for the nodes to be ready and then run these Gradle tasks in two other terminals

    ./gradlew runLindaClientPartyA

    ./gradlew runLindaClientPartyB

Now you can interact with the tuple space. I remind you that if you want to check the node's vault you can use 

    run vaultQuery contractStateType: linda.TupleSpaceState

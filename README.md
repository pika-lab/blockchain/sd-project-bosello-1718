# Hyperledger and Corda exploration
by Michael Bosello

## Project description

### Blockchain Analysis and Linda implementation

This project aims to analyze the potentials of Blockchain to solve some problems of distributed systems and bring out critical ones. Finally, we want to obtain as Proof of Concept a distributed tuple space in which, thanks to Blockchain properties, all the nodes agree on the ordering of events.

The chosen platforms are Hyperledger and Corda, for each one:
1.    Evaluate the implementation possibilities offered by APIs.
2.    Explore the interaction and coordination skills of the platforms like Push for primitives with suspensive semantics (multiple control flows).
3.    Implementation of a Linda-based tuple space and a client to interact with it.

  * Compare the two platforms under examination (possibly also with Ethereum).
  * Linda and Smart Contracts are equally expressive? Verify the expressiveness of the two models.

# Report
In order to consolidate what i've learned and to write down the analysis of above questions, i produced a report (in Italian) available in the project wiki https://gitlab.com/pika-lab/blockchain/sd-project-bosello-1718/wikis/Report

# Hyperledger fabric

## Quick start
See [Fabric Tutorial](hyperledger/TUTORIAL.md)

## [Fabric License](hyperledger/LICENSE)

Basic-network, chaincode template, javascript template and some script come from **Hyperledger Fabric Samples** and are adapted for our educational purposes.

###  <a name="license">Hyperledger Fabric License</a>

Hyperledger Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](hyperledger/LICENSE) file. Hyperledger Project documentation files are made available under the Creative Commons Attribution 4.0 International License (CC-BY-4.0), available at http://creativecommons.org/licenses/by/4.0/.

# Corda

## Quick start
See [Corda Tutorial](corda/TUTORIAL.md)

## [Corda License](corda/LICENCE)

templates come from **CorDapp Template** and are adapted for our educational purposes.

# Notes

- In this project we adopt [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) for repository management
    + Other resources: https://danielkummer.github.io/git-flow-cheatsheet/

- Michael, as a *developer*, shall **always** work on the `develop` branch and will not have write access to the `master` branch

- Every time someones starts a new task which may imply one or more commits,  he or she must **first of all** create a new branch, diverging from `develop`, named after the pattern `feature/<name-of-the-task>`, wihtout angular parentes and including the slash.
    + The aim is to easy parallel evolution of the project **and** to learn how real world project are managed

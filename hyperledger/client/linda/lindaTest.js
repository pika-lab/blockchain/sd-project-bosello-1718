'use strict';

var tupleSpace = require('./linda');

var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var operation = null;
var tupleTemplate = null;

tupleSpace.init().then( () => {
    operationCycle();
})

function operationCycle() {
    return new Promise((resolve) => {
        rl.question('Enter operation (out, in, rd, inp, rdp): ', (op) => resolve(op));
    }).then( (op) => {
        operation = op;
        return new Promise((resolve) => {
            rl.question('Enter tuple/tupleTemplate: ', (tt) => resolve(tt));
        })
    }).then( (tt) => {
        tupleTemplate = tt;
        return tupleSpace.performOperation(operation, tupleTemplate);
    }).then( tuple => {
        if(operation == "out"){
            console.log('############################################');
            console.log('Out done');
            console.log('############################################');
        } else if( (operation == "inp" || operation == "rdp") && (tuple == "") ){
            console.log('############################################');
            console.log('No matching tuple found');
            console.log('############################################');
        } else {
            console.log('############################################');
            console.log('Got tuple: ' + tuple);
            console.log('############################################');
        }
        return operationCycle();
    }).catch( (err) => {
        console.error('Failed to put/take tuple successfully :: ' + err);
    });
}
'use strict';
// Modifications Michael Bosello
/*
* Copyright IBM Corp All Rights Reserved
*
* SPDX-License-Identifier: Apache-2.0
*/

var Fabric_Client = require('fabric-client');
var path = require('path');

var fabric_client = new Fabric_Client();
var channel = fabric_client.newChannel('mychannel');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
console.log('Store path:' + store_path);

let eventHub = channel.newChannelEventHub('localhost:7051');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
let event_monitor = Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('user1', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    eventHub.connect(true);

    return new Promise((resolve) => {
        var regid = eventHub.registerChaincodeEvent('events', 'eventSender',
            (event, block_num, txnid, status) => {
                // This callback will be called when there is a chaincode event name
                // within a block that will match on the second parameter in the registration
                // from the chaincode with the ID of the first parameter.
                console.log('Successfully got a chaincode event with transid:' + txnid + ' with status:' + status);

                // to see the event payload, the eventHub must be connected(true)
                let event_payload = event.payload.toString('utf8');
                console.log('Payload: ' + event_payload);

                resolve(event_payload);
            }, (error) => {
                console.log('Failed to receive the chaincode event ::' + error);
            })
    })
});

event_monitor.then(event => {
    console.log('Got event: ' + event);
}).catch((err) => {
    console.error('Failed to receive event successfully :: ' + err);
});


// keep node.js alive
(function wait() {
    setTimeout(wait, 1000);
})();
# Modifications Michael Bosello

#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
LANGUAGE="golang"

# kill any stale or active containers
dockeractivecontainer=$(docker ps -aq)
if [[ -n "$dockeractivecontainer" ]]; then
    docker rm -f "$dockeractivecontainer"
fi
# clear any cached networks
docker network prune -f
# clean the keystore
rm -rf ./hfc-key-store

# Exit on first error
set -e

# install node dependencies 
npm install

# launch network; create channel and join peer to channel
cd ../basic-network
./start.sh

# get chaincode name
ccname=$1
if [[ -n "$ccname" ]]; then
    echo instantiate "$ccname"
else
    echo ERROR: Missing arguments. Please pass cc name.
    exit 1
fi

CC_SRC_PATH=github.com/"$ccname"

# now launch the CLI container in order to install, instantiate chaincode
docker-compose -f ./docker-compose.yml up -d cli

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode install -n "$ccname" -v 1.2.4 -p "$CC_SRC_PATH" -l "$LANGUAGE"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode instantiate -o orderer.example.com:7050 -C mychannel -n "$ccname" -l "$LANGUAGE" -v 1.2.4 -c '{"Args":[""]}' -P "OR ('Org1MSP.member','Org2MSP.member')"

# if needed invoke initialization function on chaincode
invokefuncname=$3
if [[ $2 == "-i" ]] && [[ -n "$invokefuncname" ]]; then
    sleep 10
    docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli peer chaincode invoke -o orderer.example.com:7050 -C mychannel -n "$ccname" -c '{"function":"'"$invokefuncname"'","Args":[""]}'
fi

printf "\nTotal setup execution time : $(($(date +%s) - starttime)) secs ...\n\n\n"

printf "Prepare admin and user certificates."

node ../client/enrollAdmin.js
node ../client/registerUser.js "user1"
node ../client/registerUser.js "user2"
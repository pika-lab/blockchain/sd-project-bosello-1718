# execution environment
## Step by step guide from fresh enviroment
These steps are tested on a clean installation of *Manjaro Linux*  
With pacman install the following package: 
(If you have another package manager or you want to use installers, look for those appropriate for you)  
`git` (to clone this repository)  
`base-devel` (install gcc make and so on)  
`curl`  
`python`  
`go`  
`nodejs-lts-carbon` (Carbon denote 8.X release)  
`npm`  
`docker`  
`docker-compose`  

clone this repository  
adding the current user to the docker group (needed to tun docker command without sudo) then
completely log out of your account and log back in (if in doubt, reboot)  

    sudo usermod -a -G docker $USER  

After restart  
Run docker deamon:

    sudo dockerd

Execute the following command from within the directory *hyperledger*  

    curl -sSL http://bit.ly/2ysbOFE | bash -s 1.2.0 -s  

From **./client** folder run `./startFabric.sh linda`.
Use `node ./linda/lindaTest.js` to interact with the tuple space
  
The following text run through the steps again in detail

## Install Prerequisites
Please check out prerequisites for Hyperledger Fabric at https://hyperledger-fabric.readthedocs.io/en/release-1.2/prereqs.html
## Install Binaries and Docker Images
Execute the following command from within the directory of the project to get the platform-specific binaries you will need to start up your network.

`curl -sSL http://bit.ly/2ysbOFE | bash -s 1.2.0 -s`

See [Download Platform-specific Binaries](https://hyperledger-fabric.readthedocs.io/en/release-1.2/install.html#) section for more information.

<br>

# Execution

## Hello World
From **./client** folder run `./startFabric.sh helloworld -i inithello`.
This command will clean docker and certificates, install the Fabric node dependencies for the applications,
it will spin up our various Fabric entities within the network and instantiate the selected chaincode, finally it will enroll the admin and a user into the network.

Now you can query the blockchain with `node ./helloworld/queryhelloworld.js`

## Events
From **./client** folder run `./startFabric.sh events`.
Use `node ./events/listenEvents.js` for listen and view events.
In another console use `node ./events/sendEvents.js`for produce chaincode events.

## Linda
From **./client** folder run `./startFabric.sh linda`.
Use `node ./linda/lindaTest.js` to interact with the tuple space

#### Note
Start point:
https://hyperledger-fabric.readthedocs.io/en/release-1.2/write_first_app.html

# Note
If you delete your generated config or crypto-config directory (inside ./basic-network), you'll need to purge docker images. 

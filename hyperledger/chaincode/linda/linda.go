package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type SmartContract struct{}

type Tuple struct {
	Content             string   `json:"content"`
	Quantity            int      `json:"quantity"`
	WaitingTransactions []string `json:"waitingTransactions"`
}

type Event struct {
	Content    string   `json:"content"`
	Recipients []string `json:"recipients"`
}

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
	function, args := APIstub.GetFunctionAndParameters()
	if function == "out" {
		return s.out(APIstub, args)
	} else if function == "in" {
		return s.in(APIstub, args)
	} else if function == "rd" {
		return s.rd(APIstub, args)
	} else if function == "inp" {
		return s.inp(APIstub, args)
	} else if function == "rdp" {
		return s.rdp(APIstub, args)
	}
	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) out(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	tupleAsBytes, _ := APIstub.GetState(args[0])
	tuple := Tuple{}
	event := Event{}
	event.Content = args[0]
	event.Recipients = append(event.Recipients, APIstub.GetTxID())

	if tupleAsBytes != nil {
		json.Unmarshal(tupleAsBytes, &tuple)
		tuple.Quantity = tuple.Quantity + 1
		for len(tuple.WaitingTransactions) > 0 && tuple.Quantity > 0 {
			event.Recipients = append(event.Recipients, tuple.WaitingTransactions[0][3:])
			if tuple.WaitingTransactions[0][:3] == "in-" {
				tuple.Quantity = tuple.Quantity - 1
			}
			tuple.WaitingTransactions = tuple.WaitingTransactions[1:]
		}
	} else {
		tuple.Content = args[0]
		tuple.Quantity = 1
	}

	eventAsBytes, _ := json.Marshal(event)
	APIstub.SetEvent(event.Content, eventAsBytes)

	tupleAsBytes, _ = json.Marshal(tuple)
	APIstub.PutState(args[0], tupleAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) in(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	tupleAsBytes, _ := APIstub.GetState(args[0])
	tuple := Tuple{}

	if tupleAsBytes != nil {
		json.Unmarshal(tupleAsBytes, &tuple)
		if tuple.Quantity > 0 {
			tuple.Quantity = tuple.Quantity - 1

			event := Event{}
			event.Content = args[0]
			event.Recipients = append(event.Recipients, APIstub.GetTxID())
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		} else {
			tuple.WaitingTransactions = append(tuple.WaitingTransactions, "in-"+APIstub.GetTxID())
		}
	} else {
		tuple.Content = args[0]
		tuple.Quantity = 0
		tuple.WaitingTransactions = append(tuple.WaitingTransactions, "in-"+APIstub.GetTxID())
	}

	tupleAsBytes, _ = json.Marshal(tuple)
	APIstub.PutState(args[0], tupleAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) rd(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	tupleAsBytes, _ := APIstub.GetState(args[0])
	tuple := Tuple{}

	if tupleAsBytes != nil {
		json.Unmarshal(tupleAsBytes, &tuple)
		if tuple.Quantity > 0 {
			event := Event{}
			event.Content = args[0]
			event.Recipients = append(event.Recipients, APIstub.GetTxID())
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		} else {
			tuple.WaitingTransactions = append(tuple.WaitingTransactions, "rd-"+APIstub.GetTxID())
		}
	} else {
		tuple.Content = args[0]
		tuple.Quantity = 0
		tuple.WaitingTransactions = append(tuple.WaitingTransactions, "rd-"+APIstub.GetTxID())
	}

	tupleAsBytes, _ = json.Marshal(tuple)
	APIstub.PutState(args[0], tupleAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) inp(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	tupleAsBytes, _ := APIstub.GetState(args[0])
	tuple := Tuple{}
	event := Event{}
	event.Recipients = append(event.Recipients, APIstub.GetTxID())

	if tupleAsBytes != nil {
		json.Unmarshal(tupleAsBytes, &tuple)
		if tuple.Quantity > 0 {
			tuple.Quantity = tuple.Quantity - 1

			event.Content = args[0]
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		} else {
			event.Content = ""
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		}
	} else {
		tuple.Content = args[0]
		tuple.Quantity = 0

		event.Content = ""
		eventAsBytes, _ := json.Marshal(event)
		APIstub.SetEvent(event.Content, eventAsBytes)
	}

	tupleAsBytes, _ = json.Marshal(tuple)
	APIstub.PutState(args[0], tupleAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) rdp(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	tupleAsBytes, _ := APIstub.GetState(args[0])
	tuple := Tuple{}
	event := Event{}
	event.Recipients = append(event.Recipients, APIstub.GetTxID())

	if tupleAsBytes != nil {
		json.Unmarshal(tupleAsBytes, &tuple)
		if tuple.Quantity > 0 {
			event.Content = args[0]
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		} else {
			event.Content = ""
			eventAsBytes, _ := json.Marshal(event)
			APIstub.SetEvent(event.Content, eventAsBytes)
		}
	} else {
		tuple.Content = args[0]
		tuple.Quantity = 0

		event.Content = ""
		eventAsBytes, _ := json.Marshal(event)
		APIstub.SetEvent(event.Content, eventAsBytes)
	}

	tupleAsBytes, _ = json.Marshal(tuple)
	APIstub.PutState(args[0], tupleAsBytes)

	return shim.Success(nil)
}

func main() {
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
